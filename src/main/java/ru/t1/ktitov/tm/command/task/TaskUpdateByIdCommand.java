package ru.t1.ktitov.tm.command.task;

import ru.t1.ktitov.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task-update-by-id";

    public static final String DESCRIPTION = "Update task by id";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        getTaskService().updateById(id, name, description);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
