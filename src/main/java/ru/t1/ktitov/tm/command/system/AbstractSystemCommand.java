package ru.t1.ktitov.tm.command.system;

import ru.t1.ktitov.tm.api.service.ICommandService;
import ru.t1.ktitov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
