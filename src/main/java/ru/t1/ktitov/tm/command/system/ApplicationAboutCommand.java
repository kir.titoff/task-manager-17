package ru.t1.ktitov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String NAME = "about";

    public static final String ARGUMENT = "-a";

    public static final String DESCRIPTION = "Show developer info";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Titov Kirill");
        System.out.println("E-mail: kir.titoff@gmail.com");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
